import React from 'react';
import logo from './logo.svg';
import './App.css';
import { BrowserRouter as Router, Route, Link ,NavLink} from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";

import viewList from './Components/viewList';
import updateList from './Components/updateList';
import createList from './Components/createList';

function App() {
  return (
    <Router>
      <div className="container">
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
          <img src={logo} width="30" height="30" alt="CodingTheSmartWay.com" />
          <Link to="/" className="navbar-brand">React CRUD Application</Link>
          <div className="collpase navbar-collapse">
            <ul className="navbar-nav mr-auto">
              <li className="navbar-item">
                <NavLink to="/" activeStyle={{
                  fontWeight: "bold",
                  color: "green"
                }}className="nav-link">Lists</NavLink>
              </li>
              <li className="navbar-item">
                <NavLink to="/create" activeStyle={{
                  fontWeight: "bold",
                  color: "green"
                }} className="nav-link">CreateList</NavLink>
              </li>
            </ul>
          </div>
        </nav>
        <br />
        <Route path="/" exact component={viewList} />
        <Route path="/edit/:id" component={updateList} />
        <Route path="/create" component={createList} />
      </div>
    </Router>
  );
}

export default App;
