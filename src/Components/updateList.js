import React, { Component } from 'react'
import axios from 'axios';

export default class updateList extends Component {
    constructor(props) {
        super(props);

        this.onChangebookname = this.onChangebookname.bind(this);
        this.onChangebookautor = this.onChangebookautor.bind(this);
        this.onChangebookrating = this.onChangebookrating.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

        this.state = {
            book_name: '',
            book_author: '',
            book_rating: '',
        }
    }
    

    componentDidMount() {
        axios.get('http://localhost:4000/lists/'+this.props.match.params.id)
            .then(response => {
                this.setState({
                    book_name: response.data.book_name,
                    book_author: response.data.book_author,
                    book_rating: response.data.book_rating,
                })   
            })
            .catch(function (error) {
                console.log(error);
            })
    }
    onChangebookname(e) {
        this.setState({
            book_name: e.target.value
        });
    }

    onChangebookautor(e) {
        this.setState({
            book_author: e.target.value
        });
    }

    onChangebookrating(e) {
        this.setState({
            book_rating: e.target.value
        });
    }
    onSubmit(e) {
        e.preventDefault();
        const obj = {
            book_name: this.state.book_name,
            book_author: this.state.book_author,
            book_rating: this.state.book_rating,
        };
        console.log(obj);
        axios.post('http://localhost:4000/lists/update/'+this.props.match.params.id, obj)
            .then(res => console.log(res.data));
        
        this.props.history.push('/');
    }


    render() {
        return (
            <div>
                <h3 align="center">Update Book Details</h3>
                <form onSubmit={this.onSubmit}>
                    <div className="form-group"> 
                        <label>Book Name: </label>
                        <input  type="text"
                                className="form-control"
                                value={this.state.book_name}
                                onChange={this.onChangebookname}
                                />
                    </div>
                    <div className="form-group">
                        <label>Author: </label>
                        <input 
                                type="text" 
                                className="form-control"
                                value={this.state.book_author}
                                onChange={this.onChangebookautor}
                                />
                    </div>
                    <div className="form-group">
                        <div className="form-check form-check-inline">
                            <input  className="form-check-input" 
                                    type="radio" 
                                    name="priorityOptions" 
                                    id="priorityLow" 
                                    value="Low"
                                    checked={this.state.book_rating==='Low'} 
                                    onChange={this.onChangebookrating}
                                    />
                            <label className="form-check-label">Low</label>
                        </div>
                        <div className="form-check form-check-inline">
                            <input  className="form-check-input" 
                                    type="radio" 
                                    name="priorityOptions" 
                                    id="priorityMedium" 
                                    value="Medium" 
                                    checked={this.state.book_rating==='Medium'} 
                                    onChange={this.onChangebookrating}
                                    />
                            <label className="form-check-label">Medium</label>
                        </div>
                        <div className="form-check form-check-inline">
                            <input  className="form-check-input" 
                                    type="radio" 
                                    name="priorityOptions" 
                                    id="priorityHigh" 
                                    value="High" 
                                    checked={this.state.book_rating==='High'} 
                                    onChange={this.onChangebookrating}
                                    />
                            <label className="form-check-label">High</label>
                        </div>
                    </div>
                      <br />
                    <div className="form-group">
                        <input type="submit" value="Update Todo" className="btn btn-primary" />
                    </div>
                </form>
            </div>
        )
    }
}
