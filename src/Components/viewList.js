import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
const List = props => (
    <tr>
        <td>{props.list.book_name}</td>
        <td>{props.list.book_author}</td>
        <td>{props.list.book_rating}</td>
        <td style={{ textAlign: "center" }}>
            <Link to={"/edit/" + props.list._id}>Edit</Link>
        </td>
        <td>
            <Link to={props.list._id}>Delete</Link>
            {/* <button onClick={this.delete} className="btn btn-danger">Delete</button> */}
        </td>
    </tr>
)


export default class viewList extends Component {
    constructor(props) {
        super(props);
        this.state = { lists: [] };
    }

   
    componentDidMount() {
        axios.get('http://localhost:4000/lists/')
            .then(response => {
                this.setState({ lists: response.data });
            })
            .catch(function (error) {
                console.log(error);
            })
                }
    showList() {
        return this.state.lists.map(function (currentList, i) {
            return <List list={currentList} key={i} />;
        })
    }
    delete(){
        axios.delete('http://localhost:4000/lists/'+ this.props.match.params.id)
        .then(console.log("Deleted"))
        .catch(function (error) {
            console.log(error);
        });
    }
    render() {
        return (
            <div>
                <h3>Book Details</h3>
                <table className="table table-striped" style={{ marginTop: 20 }} >
                    <thead>
                        <tr>
                            <th>Book Name</th>
                            <th>Author Name</th>
                            <th>Rating</th>
                            <th style={{ textAlign: "center" }}>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.showList()}
                    </tbody>
                </table>
            </div>
        )
    }
}
