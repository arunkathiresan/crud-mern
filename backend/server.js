const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');
const listRoutes = express.Router();

const PORT = 4000;
let List = require('./Models/list');

app.use(cors());
app.use(bodyParser.json());

mongoose.connect('mongodb://127.0.0.1/crudapp', {
});
listRoutes.route('/').get(function (req, res) {
    List.find(function (err, lists) {
        if (err) {
            console.log(err);
        } else {
            res.json(lists);
        }
    });
});

listRoutes.route('/:id').get(function (req, res) {
    let id = req.params.id;
    List.findById(id, function (err, list) {
        res.json(list);
    });
});

listRoutes.route('/update/:id').post(function (req, res) {
    List.findById(req.params.id, function (err, list) {
        if (!list)
            res.status(404).send("data is not found");
        else
            list.book_name = req.body.book_name;
        list.book_author = req.body.book_author;
        list.book_rating = req.body.book_rating;
        list.save().then(todo => {
            res.json('List updated!');
        })
            .catch(err => {
                res.status(400).send("Update not possible");
            });
    });
});

listRoutes.route('/add').post(function (req, res) {
    let list = new List(req.body);
    list.save()
        .then(List => {
            res.status(200).json({ 'list': 'List added successfully' });
        })
        .catch(err => {
            res.status(400).send('adding new List failed');
        });
});
listRoutes.route('/:id').delete(function (req, res) {
    List.deleteOne({ _id: req.params.id })
        .then(result => {
               res.status(200).json({ message: "Deletion successful!" });
        })
          .catch(err=>{
            res.status(400).json({ message: "Not succesfully delted" });
          });
});

app.use('/lists', listRoutes);

app.listen(PORT, function () {
    console.log("your application is running on:" + PORT);
})