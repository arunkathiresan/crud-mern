const mongoose=require("mongoose");
const Schema=mongoose.Schema;
let list=new Schema({
    book_name:{
        type:String
    },
    book_author:{
        type:String
    },
    book_rating:{
        type:String
    }
});

module.exports=mongoose.model('list',list);